#!/bin/bash

echo "Enter Project name"
read var

dir=$(pwd)
DIRECTORY="${dir}/${var}"

echo "Project will be created at-->"  $dir


echo "1) Y"
echo "2) N"

read -p "#?" choice


if [[ $choice -eq 2 ]]; then
	
	echo "Project is not created"
	
	tree $(pwd)


elif [[ $choice -eq 1 ]]; then
		
		

		mkdir -p $DIRECTORY

		echo "Successfully created!"
		cd $DIRECTORY

		mkdir -p css
		touch css/style.css

		mkdir -p img

		touch $DIRECTORY/index.css


		mkdir -p js
		touch js/script.js

	
		tree $DIRECTORY
		
fi

