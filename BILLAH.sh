#! /bin/bash

echo "Enter project name"
read name
echo "Project will be created at -->" $PWD
echo "1) Y"
echo "2) N"
read -p '#?' num1
styleSheet="css"
image="img"
javaScript="js"


if (($num1 == 2))
then
    echo "Project is not created"
    tree $PWD
fi

if (($num1 == 1))
then 
    echo "Successfully created!"
    mkdir $name
    cd $name/
    mkdir $styleSheet
    mkdir $image
    mkdir $javaScript

    cd $styleSheet/
    touch style.css
    cd ..
    cd $image/
    touch index.css
    cd ..
    cd $javaScript/
    touch script.js
    cd ..
    
    tree $PWD

fi




 