#!/bin/bash
echo "Enter Project Name"
read name

echo "Project will be created at-->$(pwd)"
echo -n "1) Y"
echo ""
echo -n "2) N"
echo ""

echo -n "#""?"
read choice

if [ $choice = 2 ]
then
	echo "Project is not created"
	SCRIPT=`realpath $0`
	SCRIPTPATH=`dirname $SCRIPT`
	tree $SCRIPTPATH
else
	echo "Successfully created!"
	mkdir css
	mkdir img
	mkdir js
	touch index.css
	SCRIPT=`realpath $0`
	SCRIPTPATH=`dirname $SCRIPT`
	cd $SCRIPTPATH/css
	touch style.css
	cd ..
	SCRIPT=`realpath $0`
	SCRIPTPATH=`dirname $SCRIPT`
	cd $SCRIPTPATH/js
	touch script.js
	cd ..
	SCRIPT=`realpath $0`
	SCRIPTPATH=`dirname $SCRIPT`
	tree $SCRIPTPATH
fi
